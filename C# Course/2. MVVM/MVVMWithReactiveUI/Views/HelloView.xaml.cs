﻿using System.Reactive.Disposables;
using MVVMWithReactiveUI.ViewModels;
using ReactiveUI;

namespace MVVMWithReactiveUI.Views
{
    /// <summary>
    ///     Interaction logic for HelloView.xaml
    /// </summary>
    public partial class HelloView : IViewFor<HelloViewModel>
    {
        public HelloView()
        {
            InitializeComponent();
            ViewModel = new HelloViewModel();
            this.WhenActivated(d =>
            {
                this.OneWayBind(ViewModel,
                        vm => vm.ButtonName,
                        v => v.Button.Content)
                    .DisposeWith(d);

                this.OneWayBind(ViewModel,
                        vm => vm.Stopwatch,
                        v => v.TextBlock.Text)
                    .DisposeWith(d);

                this.BindCommand(ViewModel,
                        vm => vm.ButtonCommand,
                        v => v.Button,
                        nameof(Button.Click))
                    .DisposeWith(d);
            });
        }

        object? IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (HelloViewModel) value!;
        }

        public HelloViewModel? ViewModel { get; set; }
    }
}