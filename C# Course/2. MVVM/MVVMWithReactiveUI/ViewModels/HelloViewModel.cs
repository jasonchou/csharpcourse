﻿using System;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace MVVMWithReactiveUI.ViewModels
{
    public class HelloViewModel : ReactiveObject
    {
        private CompositeDisposable _compositeDisposable;
        private DateTime _startTime;


        public HelloViewModel()
        {
            ButtonName = "Press me!!";
            Stopwatch = "00:00:000";
            ButtonCommand = ReactiveCommand.Create(() =>
            {
                _compositeDisposable?.Dispose();
                _compositeDisposable = new CompositeDisposable();
                _startTime = DateTime.Now;
                Observable
                    .Interval(TimeSpan.FromMilliseconds(10))
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Do(x =>
                    {
                        var now = DateTime.Now;
                        var timespan = now - _startTime;
                        Stopwatch = $"{timespan.Minutes:00}:{timespan.Seconds:00}:{timespan.Milliseconds:000}";
                    })
                    .Subscribe()
                    .DisposeWith(_compositeDisposable);
            });
        }

        [Reactive] public string ButtonName { get; set; }

        [Reactive] public ReactiveCommand<Unit, Unit> ButtonCommand { get; set; }

        [Reactive] public string Stopwatch { get; set; }
    }
}