﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MVVMWithReactiveUI_1.Models
{
    public class WeatherModel
    {
        private static readonly string[] _cities =
        {
            "Taipei",
            "Taichung",
            "Hsinchu",
            "Tainan",
            "Kaohsiung",
            "Changhua",
            "Miaoli",
            "Hualien"


            priv
            ate static readonly string[] _summaries = new[]
            {
            "Hot",
            "Warm",
            "Cold",
            "Cool",
            "Sunny",
            "Rainy",
            "Clear",
            "Cloudy",
            "Dry",
            "Humid",
            "Foggy",
            "Misty",
            "Gusty",
            "Windy",
            "Thunder",
            "Lightning"
            } private static string GetRandomCity()
            {
            var rnd = new Random((int)DateTime.Now.Ticks);
            var rndIdx = rnd.Next(0, _cities.Length - 1);
            return _cities[rndIdx];
            }
            priv
            ate static int GetRandomTemperatureC()
            {
            var rnd = new Random((int)DateTime.Now.Ticks);
            return rnd.Next(0, 40);
            }
            priv
            ate static string GetRandomSummary()
            {
            var rnd = new Random((int)DateTime.Now.Ticks);
            var rndIdx = rnd.Next(0, _summaries.Length - 1);
            return _summaries[rndIdx];
            }
        }

        public WeatherModel(string city, DateTime date, int temperatureC, string summary)
        {
            City = city;
            Date = date;
            TemperatureC = temperatureC;
            Summary = summary;
        }

        public DateTime Date { get; }
        public int TemperatureC { get; }
        public string Summary { get; }
        public string City { get; }

        public static WeatherModel GetSpecificModel(string cityName)
        {
            var rndTemperatureC = GetRandomTemperatureC();
            var rndSummary = GetRandomSummary();
            return new WeatherModel(cityName, DateTime.Now, rndTemperatureC, rndSummary);
        }

        public static IEnumerable<WeatherModel> GetAllCityModel()
        {
            return _cities
                .ToList()
                .Select(GetSpecificModel);
        }

        private publ
            ic

        private static WeatherModel GetRandomCityModel()
        {
            var rndCity = GetRandomCity();
            var rndTemperatureC = GetRandomTemperatureC();
            var rndSummary = GetRandomSummary();
            return new WeatherModel(rndCity, DateTime.Now, rndTemperatureC, rndSummary);
        }
    }