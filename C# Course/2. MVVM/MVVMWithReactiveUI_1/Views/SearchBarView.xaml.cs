﻿using System.Reactive.Disposables;
using MVVMWithReactiveUI_1.ViewModels;
using ReactiveUI;
using Splat;

namespace MVVMWithReactiveUI_1.Views
{
    public partial class SearchBarView : IViewFor<ISearchBarViewModel>
    {
        public SearchBarView()
        {
            InitializeComponent();
            ViewModel = Locator.Current.GetService<ISearchBarViewModel>();
            this.WhenActivated(d =>
            {
                this.Bind(ViewModel,
                        vm => vm.CityName,
                        v => v.CityNameTextBox.Text)
                    .DisposeWith(d);

                this.BindCommand(ViewModel,
                        vm => vm.SearchCommand,
                        v => v.SearchButton)
                    .DisposeWith(d);

                this.BindCommand(ViewModel,
                        vm => vm.ShowAllCitiesButtonCommand,
                        v => v.ShowAllCitiesButton)
                    .DisposeWith(d);
            });
        }

        object? IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (ISearchBarViewModel) value!;
        }

        public ISearchBarViewModel? ViewModel { get; set; }
    }
}