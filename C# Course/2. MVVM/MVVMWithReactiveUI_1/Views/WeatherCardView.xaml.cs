﻿using System.Reactive.Disposables;
using MVVMWithReactiveUI_1.ViewModels;
using ReactiveUI;

namespace MVVMWithReactiveUI_1.Views
{
    public partial class WeatherCardView : IViewFor<IWeatherCardViewModel>
    {
        public WeatherCardView()
        {
            InitializeComponent();
            this.WhenActivated(d =>
            {
                this.OneWayBind(ViewModel,
                        vm => vm.City,
                        v => v.CityNameRun.Text)
                    .DisposeWith(d);

                this.OneWayBind(ViewModel,
                        vm => vm.TemperatureC,
                        v => v.TemperatureCRun.Text)
                    .DisposeWith(d);

                this.OneWayBind(ViewModel,
                        vm => vm.Date,
                        v => v.DateRun.Text,
                        date => $"{date:yyyy-MM-dd}")
                    .DisposeWith(d);

                this.OneWayBind(ViewModel,
                        vm => vm.Summary,
                        v => v.SummaryRun.Text)
                    .DisposeWith(d);
            });
        }

        object? IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (IWeatherCardViewModel) value!;
        }

        public IWeatherCardViewModel? ViewModel { get; set; }
    }
}