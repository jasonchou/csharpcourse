﻿using System.Reactive.Disposables;
using MVVMWithReactiveUI_1.ViewModels;
using ReactiveUI;
using Splat;

namespace MVVMWithReactiveUI_1.Views
{
    public partial class WeatherCardListView : IViewFor<IWeatherCardListViewModel>
    {
        public WeatherCardListView()
        {
            InitializeComponent();
            ViewModel = Locator.Current.GetService<IWeatherCardListViewModel>();
            this.WhenActivated(d =>
            {
                this.OneWayBind(ViewModel,
                        vm => vm.Items,
                        v => v.ItemsControl.ItemsSource)
                    .DisposeWith(d);
            });
        }

        object? IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (IWeatherCardListViewModel) value!;
        }

        public IWeatherCardListViewModel? ViewModel { get; set; }
    }
}