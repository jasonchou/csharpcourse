﻿using System.Windows;
using MVVMWithReactiveUI_1.Services;
using MVVMWithReactiveUI_1.ViewModels;
using MVVMWithReactiveUI_1.Views;
using ReactiveUI;
using Splat;

namespace MVVMWithReactiveUI_1
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            // Register services
            Locator.CurrentMutable.RegisterConstant<IWeatherProviderService>(new WeatherProviderService());

            // Register viewmodel
            Locator.CurrentMutable.RegisterConstant<IWeatherCardListViewModel>(new WeatherCardListViewModel());
            Locator.CurrentMutable.RegisterConstant<ISearchBarViewModel>(new SearchBarViewModel());

            // Register viewmodel with view
            Locator.CurrentMutable.Register<IViewFor<IWeatherCardViewModel>>(() => new WeatherCardView());

            var windows = new MainWindow();
            windows.Show();
        }
    }
}