﻿using System;
using DynamicData;
using MVVMWithReactiveUI_1.Models;

namespace MVVMWithReactiveUI_1.Services
{
    public class WeatherProviderService : IWeatherProviderService
    {
        private readonly SourceList<WeatherModel> _weatherModels = new();

        public void UpdateCityWeather(string city)
        {
            _weatherModels.Clear();
            _weatherModels.Add(WeatherModel.GetSpecificModel(city));
        }

        public void UpdateAllCitiesWeather()
        {
            _weatherModels.Clear();
            _weatherModels.AddRange(WeatherModel.GetAllCityModel());
        }

        public IObservable<IChangeSet<WeatherModel>> Connect()
        {
            return _weatherModels.Connect();
        }
    }
}