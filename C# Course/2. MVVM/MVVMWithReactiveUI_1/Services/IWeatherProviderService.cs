﻿using System;
using DynamicData;
using MVVMWithReactiveUI_1.Models;

namespace MVVMWithReactiveUI_1.Services
{
    public interface IWeatherProviderService
    {
        void UpdateCityWeather(string city);
        void UpdateAllCitiesWeather();
        IObservable<IChangeSet<WeatherModel>> Connect();
    }
}