﻿using System.Reactive;
using System.Reactive.Linq;
using MVVMWithReactiveUI_1.Services;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Splat;

namespace MVVMWithReactiveUI_1.ViewModels
{
    public class SearchBarViewModel : ReactiveObject, ISearchBarViewModel
    {
        private readonly IWeatherProviderService? _weatherProviderService;

        public SearchBarViewModel(IWeatherProviderService weatherProviderService = null)
        {
            _weatherProviderService = weatherProviderService ?? Locator.Current.GetService<IWeatherProviderService>();
            var canExe = this.WhenAnyValue(x => x.CityName)
                .Select(x => !string.IsNullOrEmpty(x));

            SearchCommand =
                ReactiveCommand.Create(() => { _weatherProviderService.UpdateCityWeather(CityName); }, canExe);
            ShowAllCitiesButtonCommand = ReactiveCommand.Create(() =>
            {
                _weatherProviderService.UpdateAllCitiesWeather();
            });
        }

        [Reactive] public string CityName { get; set; }
        public ReactiveCommand<Unit, Unit> SearchCommand { get; }
        public ReactiveCommand<Unit, Unit> ShowAllCitiesButtonCommand { get; }
    }
}