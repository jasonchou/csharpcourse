﻿using System.Collections.ObjectModel;

namespace MVVMWithReactiveUI_1.ViewModels
{
    public interface IWeatherCardListViewModel
    {
        ReadOnlyObservableCollection<IWeatherCardViewModel> Items { get; }
    }
}