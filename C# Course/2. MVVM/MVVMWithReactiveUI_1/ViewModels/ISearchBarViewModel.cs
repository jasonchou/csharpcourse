﻿using System.Reactive;
using ReactiveUI;

namespace MVVMWithReactiveUI_1.ViewModels
{
    public interface ISearchBarViewModel
    {
        string CityName { get; set; }
        ReactiveCommand<Unit, Unit> SearchCommand { get; }
        ReactiveCommand<Unit, Unit> ShowAllCitiesButtonCommand { get; }
    }
}