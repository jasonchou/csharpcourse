﻿using System;
using MVVMWithReactiveUI_1.Models;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace MVVMWithReactiveUI_1.ViewModels
{
    public class WeatherCardViewModel : ReactiveObject, IWeatherCardViewModel
    {
        public WeatherCardViewModel(WeatherModel weatherModel)
        {
            Date = weatherModel.Date;
            TemperatureC = weatherModel.TemperatureC;
            Summary = weatherModel.Summary;
            City = weatherModel.City;
        }

        [Reactive] public DateTime Date { get; set; }
        [Reactive] public int TemperatureC { get; set; }
        [Reactive] public string Summary { get; set; }
        [Reactive] public string City { get; set; }
    }
}