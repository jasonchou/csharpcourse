﻿using System;
using System.Collections.ObjectModel;
using System.Reactive.Linq;
using DynamicData;
using MVVMWithReactiveUI_1.Services;
using ReactiveUI;
using Splat;

namespace MVVMWithReactiveUI_1.ViewModels
{
    public class WeatherCardListViewModel : ReactiveObject, IWeatherCardListViewModel
    {
        private readonly ReadOnlyObservableCollection<IWeatherCardViewModel> _items;
        private readonly IWeatherProviderService? _weatherProviderService;

        public WeatherCardListViewModel(IWeatherProviderService weatherProviderService = null)
        {
            _weatherProviderService = weatherProviderService ?? Locator.Current.GetService<IWeatherProviderService>();
            _weatherProviderService
                .Connect()
                .Transform(x => (IWeatherCardViewModel) new WeatherCardViewModel(x))
                .ObserveOn(RxApp.MainThreadScheduler)
                .Bind(out _items)
                .Subscribe();
        }

        public ReadOnlyObservableCollection<IWeatherCardViewModel> Items => _items;
    }
}