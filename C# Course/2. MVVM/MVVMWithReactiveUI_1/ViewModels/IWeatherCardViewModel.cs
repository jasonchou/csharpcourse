﻿using System;

namespace MVVMWithReactiveUI_1.ViewModels
{
    public interface IWeatherCardViewModel
    {
        DateTime Date { get; }
        int TemperatureC { get; }
        string Summary { get; }
        string City { get; }
    }
}