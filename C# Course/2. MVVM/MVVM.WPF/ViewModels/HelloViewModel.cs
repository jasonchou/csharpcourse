﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Timers;
using System.Windows.Input;

namespace MVVM.WPF.ViewModels
{
    public class HelloViewModel : INotifyPropertyChanged
    {
        private ICommand _buttonCommand;
        private string _buttonName;
        private DateTime _startTime;
        private string _stopwatch;
        private readonly Timer _timer = new();

        public HelloViewModel()
        {
            ButtonName = "Press me!!";
            Stopwatch = "00:00:000";
            _timer.Interval = 10;
            _timer.Elapsed += Timer_Elapsed;
            ButtonCommand = new RelayCommand(() =>
            {
                _timer.Stop();
                _startTime = DateTime.Now;
                _timer.Start();
            });
        }

        public string ButtonName
        {
            get => _buttonName;
            set
            {
                if (value == _buttonName) return;
                _buttonName = value;
                OnPropertyChanged();
            }
        }

        public ICommand ButtonCommand
        {
            get => _buttonCommand;
            set
            {
                if (Equals(value, _buttonCommand)) return;
                _buttonCommand = value;
                OnPropertyChanged();
            }
        }

        public string Stopwatch
        {
            get => _stopwatch;
            set
            {
                if (value == _stopwatch) return;
                _stopwatch = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            var now = DateTime.Now;
            var timespan = now - _startTime;
            Stopwatch = $"{timespan.Minutes:00}:{timespan.Seconds:00}:{timespan.Milliseconds:000}";
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class RelayCommand : ICommand
    {
        private readonly Func<bool> canExecute;
        private readonly Action execute;

        public RelayCommand(Action execute) : this(execute, null)
        {
        }

        public RelayCommand(Action execute, Func<bool> canExecute)
        {
            this.execute = execute ?? throw new ArgumentNullException(nameof(execute));
            this.canExecute = canExecute;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                if (canExecute != null)
                    CommandManager.RequerySuggested += value;
            }
            remove
            {
                if (canExecute != null)
                    CommandManager.RequerySuggested -= value;
            }
        }

        public bool CanExecute(object parameter)
        {
            return canExecute?.Invoke() ?? true;
        }

        public void Execute(object parameter)
        {
            execute();
        }
    }
}