﻿using System.Windows.Controls;
using MVVM.WPF.ViewModels;

namespace MVVM.WPF.Views
{
    /// <summary>
    ///     Interaction logic for HelloView.xaml
    /// </summary>
    public partial class HelloView : UserControl
    {
        public HelloView()
        {
            InitializeComponent();
            DataContext = new HelloViewModel();
        }
    }
}